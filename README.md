# Quico

[![](http://img.youtube.com/vi/Ne4RNotvTO0/0.jpg)](http://www.youtube.com/watch?v=Ne4RNotvTO0 "quico")

Quico (*/ˈkiko/*), short for Kids Console (or Kids Computer). Also, one of the main
characters of [El Chavo del Ocho][chavo]

[chavo]: https://es.wikipedia.org/wiki/El_Chavo_del_8

## Goal

Build an inexpensive retro-modern video-game console, aimed for kids.
Easy to program, easy to assemble.

## Features

* 64x32 LED Matrix display
* Gamepad support
* Accelerometer
* Chiptune music
* CircuitPython powered
* Portable
* Open source / open hardware

## Design

This is still work-in-progress. The design is still evolving. But so far, this
is the latest design:

![design](https://docs.google.com/drawings/d/e/2PACX-1vQajPfEYPLE8LTM3UsddkS9KzMxVFFYKXvqcGyf8DAQf5KnckxyKH-2W2TfDHCqToQfZMXLVWvuXbwX/pub?w=971&h=663)

## Parts

![parts](https://lh3.googleusercontent.com/pw/ACtC-3ehXagSfw4_d4GshrCSb8OsJ6Ro1M9sZjthuA3nbOVnnej1jXkUKSAJlqFnR6hUObkApTw2OYqwGvw48N9ADuytv7oV9raeanZThrELtKN9KlM8x9aqWIAx9hXA5ccIlSoBf64SOmdoVZz6TTVPbzTFew=-no)

* A: [64×32 LED Matrix display][ledmatrix]
* B: [Adafruit MatrixPortal M4][matrixportal]
* C: [Music shield][shield] for the MatrixPortal M4
* D: Bluetooth gamepad ([any modern Bluetooh gamepad will work…][gamepads])
* E: [Audio amplifier][amplifier]
* F: [8 Ohm speaker][speaker]

[ledmatrix]: https://www.adafruit.com/product/2279
[matrixportal]: https://www.adafruit.com/product/4745
[shield]: https://gitlab.com/ricardoquesada/quico/-/blob/master/docs/shield_76489.md
[gamepads]: https://gitlab.com/ricardoquesada/bluepad32/-/blob/master/docs/supported_gamepads.md
[amplifier]: https://www.amazon.com/gp/product/B01FDD3FYQ/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1
[speaker]: https://www.amazon.com/gp/product/B07YX9QLLN/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1

## Support & Further info

* Chat: [Discord][discord]
* [Docs][docs]
* [Blog posts about Quico][blog]

[discord]: https://discord.gg/r5aMn6Cw5q
[docs]: https://gitlab.com/ricardoquesada/quico/-/tree/master/docs
[blog]: https://retro.moe/category/quico/

## Software dependencies

These are the firmware that needs to be installed in order to have Quico running:

### Bluepad32

Bluepad32 is the firmware that controls the gamepad. It must be installed in the ESP32.
Detailed instructions here:

* [Bluepad32 for Airlift][bluepad32_airlift]

[bluepad32_airlift]: https://gitlab.com/ricardoquesada/bluepad32/-/blob/master/docs/plat_airlift.md

### CircuitPython

Quico runs with unmodified CircuitPython, but for a better REPL experience it is better to
compile CircuitPython with, `CIRCUITPY_TERMINALIO=0`. Example:

```
$ export BOARD=matrixportal_m4
$ cd src/circuitpython/ports/atmel-samd
$ make CIRCUITPY_TERMINALIO=0 -j
```

For further details see [bug #2791][bug_2791]

[bug_2791]: https://github.com/adafruit/circuitpython/issues/2791

### CircuitPython libraries

* [Adafruit CircuitPython 74HC595][Adafruit_CircuitPython_74HC595]

[Adafruit_CircuitPython_74HC595]: https://github.com/adafruit/Adafruit_CircuitPython_74HC595

## TODO

* Write games!
* Write documentation
* Volume control
* Polish, polish, polish
