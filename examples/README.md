## Quico examples

* Copy `src/bluepad32.py` and `src/music76489.py` files to the `CIRCUITPY` folder
* Choose one of the `code_*.py` examples and copy it to `CIRCUITPY` as `code.py`
* copy `data/` folder to `CIRCUITPY` folder
* Install needed CircuitPython dependencies. Open the different `code_.*py` files for further info

E.g:

```sh
# Valid for Linux
$ cp ../src/bluepad32.py ../src/music76489.py /media/$USER/CIRCUITPY
$ cp code_snake.py /media/$USER/CIRCUITPY/code.py
$ cp -r data /media/$USER/CIRCUITPY/
```
