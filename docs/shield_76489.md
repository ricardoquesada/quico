# SN76489 shield for the MatrixPortal M4

A chiptune shield for the [MatrixPortal M4][matrixm4]. Part of the [Quico Kids Console][quico] project.

It is a single SN76489 board (mono) that can be attached to the MatrixPortal M4,
and play chiptune music using CircuitPython.

[quico]: https://retro.moe/2020/12/31/building-quico-improved-sound-and-more-part-iii/

### Images

Assembled:

![assembled](https://lh3.googleusercontent.com/pw/ACtC-3d8JU3Kzz2r_wpSNG22U2taDkWwqoxAV72PW8Z6FEA5HQqB6Bx3lFg0paf9F-F4xdNhFkXrngcvgbWlcq_hBVDT3Rhi8jgTY-20IZicE82JKaItYKpKWN7p1CgZvzQQFLQm_BkirXmkJyuTCwBT3HaHGQ=-no)

Mounted as shield of the MatrixPortal M4 & mounted in the LED matrix:

![together](https://lh3.googleusercontent.com/pw/ACtC-3cMrTc8Yru8WBPJ6F6bdUHtT_JNwHC0ZUf4xlPIstOHQtOHnj2F7h5T4mosZv-XIyh9JR5eCi2PgYk4eaAJ2c0jPrrCp1K0qCXCVbXRht03nEq-GC18HnL-duHhO6vs__Yw85Oh-EjWui4tPvILAiLIGg=-no)

Parts:

![parts](https://lh3.googleusercontent.com/pw/ACtC-3fDtSu9jDwcu2Lg24WmxRTQ0A0o996Ji6jmb6JP8OT8cAeZk7rlGLw5Kb9uw8GL8o8UTaCE-7-PkTq5VtLsmvshyuoPVesOxNoH_pGW4KF1zzsRaTSJbW9eqGS8lmB-SMLVskJD9V1atMxQTrTQe0LkRg=-no)

### BOM

| What | Quantity |
| -----| ----- |
| [PCB][pcb] (D) | 1|
| [SN76489][76489] (K)| 1 |
| [74HC595][74595] (J) | 1 |
| [DIP socket 2x8][socket] (L) | 2 |
| [Ceramic Capacitor 0.1uf][cap_01] (G)| 2 |
| [Electrolytic Capacitor 470uf][cap_470] (I) | 1 |
| [Pin header female 1x8][pin_female] (B) | 1 |
| [Pin header male 1x8][pin_male] (C) | 1 |
| [3.579545MHz oscillator][oscillator] (A) | 1 |
| [3M standoff 10mm][standoff] (E) | 2 |
| [Conn Jack Stereo 3.5mm][jack] (H) <small>1</small> | 1 |
| [1x2 right-angle male header][angle] (F) <small>2</small> | 2 |
| [Audio amplifier][amplifier] (M) <small>2</small> | 1 |
| [8 Ohm speaker][speaker] (N) <small>2</small> | 1 |

* <small>1</small>: Needed for headphone output
* <small>2</small>: Needed for speakers output

### Schematic

<img src="images/schematic.png"/>


[74595]: https://www.digikey.com/en/products/detail/texas-instruments/SN74HC595NE4/1571270
[76489]: https://www.ebay.com/sch/i.html?_from=R40&_trksid=p2380057.m570.l1313&_nkw=sn76489&_sacat=0
[angle]: https://www.adafruit.com/product/1540
[cap_01]: https://www.digikey.com/en/products/detail/united-chemi-con/KVD101L104M32A0T00/6210707
[cap_470]:https://www.digikey.com/en/products/detail/UFW0J471MED1TD/493-10959-1-ND/4317883
[jack]: https://www.digikey.com/en/products/detail/cui-devices/SJ-3523-SMT-TR/669704
[oscillator]: https://www.digikey.com/en/products/detail/ecs-inc/ECS-100AX-035/827253
[pcb]: https://support.jlcpcb.com/article/102-kicad-515---generating-gerber-and-drill-files
[pin_female]: https://www.sparkfun.com/products/11895
[pin_male]: https://www.adafruit.com/product/392
[socket]: https://www.digikey.com/en/products/detail/110-93-316-41-001000/ED3316-ND/14041?itemSeq=349147687
[standoff]: https://www.amazon.com/gp/product/B07BRK411L/ref=ppx_yo_dt_b_asin_title_o01_s00?ie=UTF8&psc=1
[amplifier]: https://www.amazon.com/gp/product/B01FDD3FYQ/ref=ppx_yo_dt_b_asin_title_o05_s00?ie=UTF8&psc=1
[speaker]: https://www.amazon.com/gp/product/B07YX9QLLN/ref=ppx_yo_dt_b_asin_title_o04_s00?ie=UTF8&psc=1
[matrixm4]: https://learn.adafruit.com/adafruit-matrixportal-m4
